import React, { Component } from "react";
import logo from "../../../assets/buncis_logo_w.png";
import SidebarList from "../sidebarList/sidebarList";

class Sidebar extends Component {
  render() {
    return (
      <div className="col-2" style={{ display: this.props.status }}>
        <div className="sidebar container-fluid">
          <img id="logo" src={logo} alt="" srcset="" />
          {this.props.sidebars.map(sidebar => (
            <SidebarList sidebar={sidebar} />
          ))}
        </div>
      </div>
    );
  }
}

export default Sidebar;
